Fun with Ames, IA Housing Data
===========================

This is an attempt to compare a few different modeling techniques using some housing data accompanying a paper by Dean De Cock, in the Journal of Statistics Education. The paper proposed these data as an alternative to a classic set of Boston housing data.

The paper can be found here: http://jse.amstat.org/v19n3/decock.pdf.

The data dictionary is here: http://jse.amstat.org/v19n3/decock/DataDocumentation.txt
