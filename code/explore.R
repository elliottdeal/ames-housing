# Retrieve and explore Ames housing data.

rm(list = ls())

library(ggplot2)

# Constants ---------------------------------
# Paths:
kDataDir <- "data"
kDerivedDir <- "derived"

kRawAmesPath <- file.path(kDataDir, "AmesHousing.txt")
kAmesDictPath <- file.path(kDataDir, "DataDocumentation.txt")

# URL for Ames housing data.
kAmesDataURL <- "http://jse.amstat.org/v19n3/decock/AmesHousing.txt"
kAmesDictURL <- "http://jse.amstat.org/v19n3/decock/DataDocumentation.txt"



# Setup -----------------------------------
# Download the raw housing data if you haven't done so yet.
if(!file.exists(kRawAmesPath)) download.file(kAmesDataURL, kRawAmesPath)
if(!file.exists(kAmesDictPath)) download.file(kAmesDictURL, kAmesDictPath)

# Load the Ames data; it's tab-separated.
d <- read.csv(kRawAmesPath, sep = "\t", stringsAsFactors = FALSE)

# Check the top and bottom few rows.
head(d)
tail(d)

# Drop the periods from the names.
names(d) <- gsub("\\.", "", names(d))

# Explore ----------------------------------------------
# How much data is missing?
# Tabulate missingness, then turn into a data.frame for plotting.
missing_vars <- sort(sapply(d, function(x) mean(is.na(x))))
missing_vars <- missing_vars[missing_vars > 0]
missing_vars <- data.frame(variable = names(missing_vars),
                           prct_missing = unname(missing_vars))
# Reorder variable so that it plots nicely, in decreasing share of missingness.
missing_vars$variable <- reorder(missing_vars$variable,
                                 missing_vars$prct_missing)

# Five variables are missing more than half the time.
# However, looking at the data dictionary, the missing data all makes sense:
#   not every house has a pool, alley, etc.
ggplot(missing_vars, aes(x = variable, y = prct_missing)) +
  geom_col() +
  coord_flip() +
  theme_classic()


# Sales conditions --------------------------
# How does SalePrice look?
# They're right-skewed, as home prices generally are.
hist(d$SalePrice, breaks = 100)

# Looks fairly log-normal.
hist(log(d$SalePrice), breaks = 30)
summary(d$SalePrice)
head(d)
hist(scale(log(d$SalePrice)), breaks = 30)

# There are a variety of sales types. I probably just want to focus on "Normal"
#   transactions, at least at first, though dropping "Partial" will likely mean
#   I'm dropping many new home sales.
barplot(prop.table(table(d$SaleCondition)))
boxplot(YearBuilt ~ SaleCondition, data = d)

# There's probably some seasonality.
barplot(table(d$MoSold))

# There appear to be some low-end SalePrice outliers.
barplot(prop.table(table(d$YrSold)), main = "Year Built")
boxplot(log(SalePrice) ~ YrSold, data = d)

# Look into the low end.
# Both were sold under abnormal conditions.
d[log(d$SalePrice) < 10, ]
head(d)

# Conventional warranty deeds form the bulk of sales, though there are some
#   new homes and court officer deeds/estates as well.
barplot(table(d$SaleType))
table(d[c("SaleType", "SaleCondition")])


# Time/Age --------------------------------------------------------
# YearBuilt is fairly left-skewed, which follows intuition
hist(d$YearBuilt, breaks = 50)

# YearRemodAdd is somewhat left-skewed, with what appears to be censoring at
#   1950, possibly for homes not modeled before then...
hist(d$YearRemodAdd, breaks = 50)

quantile(d$YearRemodAdd, probs = c(seq(0, 1, .1)))

# This is definitely a variable to treat with caution.
plot(YearRemodAdd ~ YearBuilt, data = d)

# Create variables for age at time of sale and time since remodeling.
# Some homes appear to have been built after their sale, which is possible,
#   (custom builds) but something to approach with caution.
# Perhaps the homes built and sold in the same year can be assumed to be new
#   home sales.
d$Age <- d$YrSold - d$YearBuilt
hist(d$Age, breaks = 60)
summary(d$Age)

# Newer homes are generally more expensive.
plot(log(SalePrice) ~ Age, data = d)
# As are recently remodeled homes.
plot(log(SalePrice) ~ YearRemodAdd, data = d)

head(d)


# Home Characteristics -------------------------------
# Bedrooms and baths are somewhat categorical




# Land ---------------------------------------------


# Cleaning -------------------------------------------
table(d$SaleCondition)
table(d$SaleType)
# Drop any sales where SaleCondition != "Normal".
d[d$SaleCondition == "Normal", ]


